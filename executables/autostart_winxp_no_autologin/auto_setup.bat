@echo off

mkdir C:\AHKAutostart

xcopy starter.exe C:\AHKAutostart
xcopy nssm.exe C:\AHKAutostart
REM xcopy start.bat C:\AHKAutostart

pushd C:\AHKAutostart

REM nssm install AutostartAHK C:\AHKAutostart\start.bat
nssm install AutostartAHK C:\AHKAutostart\starter.exe
nssm set AutostartAHK AppDirectory C:\AHKAutostart
nssm set AutostartAHK Type SERVICE_INTERACTIVE_PROCESS
nssm set AutostartAHK AppExit 0 Exit
nssm set AutostartAHK AppExit 1 Exit