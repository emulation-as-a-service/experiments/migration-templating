import argparse
import json
import logging
import sys
import os

import GLOBALS
import git_repo
from create_iso import AutomationCategory
from eaas_client import start_client, get_client
from execute import run_environment_with_dump, run_environment_with_file_types, \
    run_environment_with_sikuli_autostart_and_iso, run_environment_with_sikuli, \
    run_environment_with_migration, run_compute_for_component_with_sikuli

logger = logging.getLogger(__name__)


class StreamToLogger(object):
    """
    Fake file-like stream object that redirects writes to a logger instance.
    """

    def __init__(self, logger, level):
        self.logger = logger
        self.level = level
        self.linebuf = ''

    def write(self, buf):
        for line in buf.rstrip().splitlines():
            self.logger.log(self.level, line.rstrip())

    def flush(self):
        pass


def set_config(config_path, config_to_set):
    with open(config_path) as conf:
        if config_to_set == "global":
            GLOBALS.global_config = json.load(conf)
        elif config_to_set == "task":
            GLOBALS.task_config = json.load(conf)


def execute_template_with_autostart():
    global file_format, parameter, folder
    logger.info("Autostart installation is active!")
    supported_os = ["os:windows:xp:32bit", "os:windows:xp:64bit", "os:windows:95",
                    "os:windows:98"]
    if not env_os or env_os not in supported_os:
        logger.info("OS is not defined for environment with id %s", env_id)
        exit(1)
    if compute_type != "migration":
        if compute_type == "dump":
            iso_type = AutomationCategory.DUMP

        elif compute_type == "fileTypes":
            iso_type = AutomationCategory.FILE_TYPES

        logger.info("Successfully read config, starting now...")
        run_environment_with_sikuli_autostart_and_iso(env_id, env_os, should_save_env,
                                                      new_env_name, iso_type,
                                                      executable,
                                                      input_file=input_file_path,
                                                      output_type=None,
                                                      input_folder="",
                                                      timeout=timeout,
                                                      sikuli_delay=90,
                                                      task_id=task_id,
                                                      delete=should_delete)
    else:
        iso_type = AutomationCategory.MIGRATION
        file_format = config["outputFileFormat"]
        parameter = config.get("outputParameter", None)

        folder = "/tmp-storage/automation/" + task_id + "/files"

        run_environment_with_sikuli_autostart_and_iso(env_id, env_os, should_save_env,
                                                      new_env_name, iso_type,
                                                      executable,
                                                      input_file=None,
                                                      output_type=file_format,
                                                      input_folder=folder,
                                                      timeout=timeout,
                                                      sikuli_delay=90,
                                                      task_id=task_id,
                                                      parameter=parameter,
                                                      delete=should_delete)


def execute_template():
    global file_format, parameter, folder
    logger.info("Autostart installation is off!")
    if compute_type == "dump":
        run_environment_with_dump(env_id, should_save_env, new_env_name, executable,
                                  input_file_path, timeout, task_id)
    elif compute_type == "migration":

        file_format = config["outputFileFormat"]
        parameter = config.get("outputParameter", None)

        folder = "/tmp-storage/automation/" + task_id + "/files"

        run_environment_with_migration(env_id, should_save_env, new_env_name,
                                       executable, file_format, timeout,
                                       input_folder=folder, task_id=task_id,
                                       parameter=parameter)

    elif compute_type == "fileTypes":
        run_environment_with_file_types(env_id, should_save_env, new_env_name,
                                        executable,
                                        input_file_path, timeout, task_id)


if __name__ == '__main__':

    my_parser = argparse.ArgumentParser(description="Automation Tasks")
    my_parser.add_argument("task_config", nargs="?")
    my_parser.add_argument("-t", dest="task_id")
    my_parser.add_argument("-c", dest="global_config")
    my_parser.add_argument("-a", dest="token")
    my_parser.add_argument("-u", dest="user_id")
    my_parser.add_argument("--sikuli", action='store_true')

    args = my_parser.parse_args()

    if os.path.exists("/tmp-storage/automation"):
        logging.basicConfig(format="%(asctime)s %(name)s - %(levelname)s: %(message)s",
                            datefmt="%d/%m/%Y %H:%M:%S,",
                            level=logging.INFO,
                            filename=f"/tmp-storage/automation/{args.task_id}/automation.log")
    else:
        logging.basicConfig(format="%(asctime)s %(name)s - %(levelname)s: %(message)s",
                            datefmt="%d/%m/%Y %H:%M:%S,",
                            level=logging.INFO)

    sys.stdout = StreamToLogger(logger, logging.INFO)
    sys.stderr = StreamToLogger(logger, logging.ERROR)

    logger.info("Creating automation log file ...")
    logger.info("Args: %s", args)
    logger.info("Starting Automation Task...")

    set_config(args.task_config, "task")

    if args.global_config:
        logger.info("Got global config!")
        set_config(args.global_config, "global")
    else:
        logger.info("Did not get global config, applying standard config.")

        GLOBALS.global_config = {
            "baseUrl": "http://127.0.0.1:8080",
            "repoUrl": "git@github.com:Aeolic/File-Format-Information.git",
            "repoDirectory": "/tmp-storage/file_repo",  # TODO actually use these
            "storageDirectory": "/tmp-storage"
        }
        GLOBALS.EMIL_BASE_URL = GLOBALS.global_config["baseUrl"] + "/emil"

    if args.token and args.user_id:
        GLOBALS.global_config["token"] = args.token
        GLOBALS.global_config["userId"] = args.user_id
        logger.info(f"Got user id: {args.user_id}")

    config = GLOBALS.task_config
    task_id = args.task_id

    logger.info("Got config:")
    logger.info(json.dumps(config, indent=4))

    start_client(GLOBALS.global_config.get("baseUrl") + "/emil",
                 GLOBALS.global_config.get("token"))

    git_repo.init_repo()

    output_type = config.get("outputType")
    should_save_env = output_type == "environment"

    if output_type not in ["environment", "files"]:
        logger.error(f"Output type {output_type} not supported!")
        exit(1)

    timeout = config.get("timeout", 10)
    env_id = config.get("environmentId", None)
    component_id = config.get("componentId", None)
    should_compute = True

    if not env_id and not component_id:
        logger.error(f"No target it was provided, exiting...")
        exit(1)

    if not component_id:
        details = get_client().get_env_details(env_id)
        env_os = details.get("os", None)
        GLOBALS.task_config["os"] = env_os

    new_env_name = ""
    if should_save_env:
        new_env_name = config["environmentName"]

    # ----- sikuli automation -----
    if args.sikuli:
        logger.info("Automation Task contains sikuli task!")

        sikuli_url = config.get("sikuliUrl")
        custom_outputs = config.get("customOutputs", None)
        sikuli_params = config.get("sikuliParams", [])

        res_x = config.get("resolutionX", None)
        res_y = config.get("resolutionY", None)

        session_id = config.get("sessionId", None)

        if not session_id:
            logger.info("No session id found!")
        else:
            logger.info(f"Got session id {session_id}, automation task is run within a network...")
            # TODO implement session deletion upon completion (success & error)

        resolution = {"x": res_x, "y": res_y} if res_x and res_y else None

        # TODO output type, currently only files are supported
        # TODO UI needs to pass env id, even for component to make this work eventually
        # --- sikuli task was started for an already running component ---
        if component_id:
            logger.info(
                f"- Task is specified for a currently running component with id {component_id}.")
            run_compute_for_component_with_sikuli(component_id, env_id, new_env_name,
                                                  should_save_env, sikuli_url,
                                                  True,
                                                  90, True, custom_outputs, task_id,
                                                  sikuli_params, resolution, timeout,
                                                  session_id=session_id)


        # --- sikuli task was started for an environment ---
        else:
            logger.info(
                f"- Task is specified for an environment with id {env_id}.")
            custom_outputs = config.get("customOutputs", None)

            logger.info("Successfully read config, executing now...")  # TODO delay as param ?
            run_environment_with_sikuli(env_id, sikuli_url, should_save_env, new_env_name,
                                        timeout, sikuli_delay=90, script_already_uploaded=True,
                                        task_id=task_id, sikuli_only=True,
                                        custom_outputs=custom_outputs, sikuli_params=sikuli_params,
                                        resolution=resolution)

    # ----- automation templates -----
    else:
        logger.info("Automation Task contains Automation Template task!")

        compute_type = config["automationType"]
        executable = config["executableLocation"]
        input_file_name = config["fileName"]
        input_file_path = config["filePath"]
        install_autostart = config.get("installAutostart", False)
        should_delete = config.get("deleteAutostart", True)

        if compute_type not in ["dump", "migration", "fileTypes"]:
            logger.info(
                f"ERROR: type {compute_type} needs to be 'dump', 'migration' or 'fileTypes'")
            exit(1)

        if not install_autostart:
            execute_template()

        else:
            execute_template_with_autostart()

    logger.info("Done with python automation script!")
