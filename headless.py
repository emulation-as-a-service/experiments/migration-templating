import datetime
import logging
import os
import tarfile
import time

from time import sleep
from eaas_client import get_client, start_client

CURRENT_COMPONENT_ID = None
SIKULI_RESULT = None

logger = logging.getLogger(__name__)


def start_component_with_compute(env_id: str, save_env: bool, new_env_name: str, timeout=120,
                                 output_drive="boot", task_id=None):
    global CURRENT_COMPONENT_ID

    start_env_req = {"type": "machine",
                     "environment": env_id,
                     "archive": "default",
                     "keyboardLayout": "us",
                     "hasOutput": not save_env,
                     "input_data": [],
                     "outputDriveId": output_drive}

    logger.info(f"Starting Component with env id: {env_id}")
    compo_response = get_client().start_component(start_env_req)
    logger.info(f"Status: {compo_response.status_code}")
    logger.info(f"Got response: {compo_response.json()}")
    component_id = compo_response.json()["id"]
    logger.info(f"Starting with component id {component_id}")

    compute_id = start_compute_for_component(component_id, env_id, new_env_name, save_env, timeout)

    return wait_and_handle_compute_result(component_id, compute_id, save_env, task_id, timeout)


def start_compute_for_component(component_id, env_id, new_env_name, save_env, timeout):
    global CURRENT_COMPONENT_ID
    CURRENT_COMPONENT_ID = component_id
    comp_spec = {"componentId": component_id, "environmentId": env_id,
                 "shouldSaveEnvironment": save_env,
                 }
    if save_env:
        comp_spec["saveEnvironmentLabel"] = new_env_name
    req = {"timeout": timeout + 5, # FIXME testing only
           "components": [comp_spec]}
    logger.info(f"Sending to compute: {req}")
    compute_response = get_client().start_compute(req)
    compute_id = compute_response.json()["id"]
    logger.info(f"Computing started, ID: {compute_id}")
    return compute_id


def wait_and_handle_compute_result(component_id, compute_id, save_env, task_id, timeout):
    global SIKULI_RESULT
    compute_response, success = wait_til_compute_done(compute_id, component_id, timeout)
    logger.info(f"Session with id {compute_id} is done!")
    if success:
        if not save_env:

            blobstore_url = compute_response["result"][0]["resultBlob"]

            logger.info(f"Got blobstore url: {blobstore_url}")

            return handle_blobstore_file_output(blobstore_url, task_id)

        else:
            result_env_id = compute_response["result"][0]["environmentId"]
            logger.info(f"Stored new env: {result_env_id}")
            logger.info(
                "Setting SIKULI RESULT, if started with SIKULI, script should now continue...")
            SIKULI_RESULT = result_env_id
            return result_env_id

    else:
        logger.info(f"Compute failed: {compute_response}")
        return "Automation Task failed! Check logs for more information." \
               "If the OS is Windows 95/98, this might be a problem where the backend could" \
               "not properly recognize the OS shutdown, which leads to a timeout." \
               "There will be a workaround eventually!"


def handle_blobstore_file_output(blobstore_url, task_id):
    global SIKULI_RESULT
    blobstore_response = get_client().get(blobstore_url)
    temp_dir = "output"
    if os.path.exists("/tmp-storage"):
        temp_dir = "/tmp-storage/automation/" + task_id + "/output"
    os.mkdir(temp_dir)
    with open(temp_dir + "/files.tgz", "wb") as f:
        f.write(blobstore_response.content)
    with tarfile.open(temp_dir + "/files.tgz", "r:gz") as zip_ref:
        zip_ref.extractall(path=temp_dir)
    logger.info(
        "Setting SIKULI RESULT, if started with SIKULI, script should now continue...")
    SIKULI_RESULT = temp_dir
    return temp_dir


#TODO don't check timeout here anymore but in sikuli exec
def wait_til_compute_done(compute_id, component_id, timeout):
    logger.info(f"Waiting til compute is done...")

    start_time = datetime.datetime.now()

    is_compute_done = False
    is_stopped = False
    while not is_compute_done:

        sleep(15)

        state_response = get_client().get_compute_status(compute_id)

        if state_response.status_code != 200:
            logger.info(f"Got status code: {state_response.status_code}")
            return "Compute API did not return 200!", False

        logger.info(f"Got response (compute state):")
        logger.info(state_response.json())

        if state_response.json().get("result"):

            state = state_response.json()["result"][0]["state"]

            if state == "STOPPED":
                logger.info(f"Done!")
                return state_response.json(), True

            else:
                logger.info(f"Computation not done yet, state: {state}")

        else:
            logger.info(f"No result/state yet...")
            now = datetime.datetime.now()
            diff = now - start_time
            logger.info(f"Diff: {diff.seconds // 60} minutes, {diff.seconds % 60} seconds.")

            if not is_stopped and (diff.seconds / 60) > timeout - 2:
                logger.info(
                    f"Component was not ready while timeout was reached, manually stopping!")
                get_client().stop_compute(compute_id)
                is_stopped = True
                # stop_response = get_client().stop_component(component_id).json()
                # if stop_response["url"]: # TODO this only works for file output, env timeout?
                #     return "Compute timed out, however got url", stop_response

            elif (diff.seconds / 60) > timeout + 3: # TODO for larger operations, a 5 min window to retrieve files might be to short
                return "Compute did not finish within the given time limit", False


if __name__ == '__main__':
    env_id = "b993f8d5-9e32-4093-8671-2cb24746d779"  # CoffeeCup
    # env_id = "65206139-ba04-4685-9a31-961031709e37"  # Win XP Base
    # env_id = "fee4edee-1c7c-4b26-b8b3-8df611ca9ee1" # iPhoto
    # env_id = "4c1aaf3f-aafe-4974-86c2-b5e803d743ac" # Win XP + Sublime + Autostart
    # env_id = "ed776b5b-0a75-4185-ae2e-f9761e1760a3"
    # env_id = "1ceafa90-912e-4153-9534-1961008359d9"  # Win XP + Sublime Installer
    # env_id = "ed776b5b-0a75-4185-ae2e-f9761e1760a3"
    # save_env = False
    # new_env_name = "flexible backend"
    # start_component_with_compute(env_id, save_env, new_env_name, 15)
    start_client("https://au-demo.stabilize.app/emil",
                 "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJQSk9TcnllS3lrbEFxWUpWTVRwTENQYmU4b25OVUtUMk9DQ09nLWhuYXBNIn0.eyJleHAiOjE2NTY3MDA5OTYsImlhdCI6MTY1NjY2NDk5NywiYXV0aF90aW1lIjoxNjU2NjY0OTk3LCJqdGkiOiJmYTA4ZDAwOC0zNTg0LTQ3YzItOWZkOS0yZGE5MmI5N2JjNjgiLCJpc3MiOiJodHRwczovL2F1LWRlbW8uc3RhYmlsaXplLmFwcC9hdXRoL3JlYWxtcy9tYXN0ZXIiLCJhdWQiOiJlYWFzIiwic3ViIjoiZjQ1MDAwMDEtZWFhNy00YTEwLTg3ZTctYmYyZThmZjE0MGU3IiwidHlwIjoiSUQiLCJhenAiOiJlYWFzIiwibm9uY2UiOiJqSk1DQW9OcXRHYUV4QTNFSFh4UVJDYkljUEt-dGN1TyIsInNlc3Npb25fc3RhdGUiOiI2ZTlkYzhhOS1lODVkLTQ1MTQtYTIwMi1jMmI2YWY2OTlkZTciLCJhdF9oYXNoIjoiNHV2VWtfX3o0NXFVZkVOa05kU0ZnUSIsImFjciI6IjEiLCJzX2hhc2giOiIxMFJiOEctRnM4dVVwMFNQNXZjbk1nIiwic2lkIjoiNmU5ZGM4YTktZTg1ZC00NTE0LWEyMDItYzJiNmFmNjk5ZGU3IiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJhZG1pbiJ9.SZri2uL_Q7YCjRbQ_4u6Zwov6hlR_JW9Ci7yLgP7OXjWm299kOM0x4srGn4I8LXHuWXhNmXULIsfuAhKFyEd_kx_q8-dNtXJIgmbYyh3f_B2AaTOxqOgYwfeb6CztLhoBC94hlYzbBXkYSQ1UCcgRPA1I_pVvqmx5F_LP31PSmWSz8wZqzi9IxIwc567ZznWMbLR9rjFhTTBLpAZiGtWHyNc2ZUiVp21FO7erOX4OnR5tl1q1HkpWWOtakC4cSGfSEELgQkmowtFCgIRKpXExInuHxkeAxpSoK1IL1o12hcQMwsN7N9tI2KPbpxzOwl1zYBe_1IXaYKp4nMI7QWv5Q")
    resp = get_client().stop_component("abfd3044-11de-4195-b550-430610e427a7")
    print(resp.json())
