import json
import logging
import time

import requests

import GLOBALS

logger = logging.getLogger(__name__)

eaas_client = None


class EaasClient:

    def __init__(self, url, token):
        logger.info("Initializing client...")
        self.url = url
        self.token = token

        if not token:
            self.token = "undefined"

        logger.info(f"Got url: {url}")
        logger.info(f"Got Token: {token}")

        self.s = requests.Session()
        if token:
            self.s.params = {"access_token": token}

    def get(self, url):
        return self.s.get(url)

    def upload_file_to_blobstore(self, path):
        logger.info(f"In upload to blobstore, file: {path}")
        try:
            file = open(path, "rb")
        except Exception as e:
            logger.info("Could not find file: " + path)
            logger.info(e)
            exit(1)

        req = self.s.post(self.url + "/upload", files={"file": file})

        logger.info(f"Got status: {req.status_code} for file {path}")
        logger.info(f"Got response:  {req.json()}")
        return req.json()["uploads"][0]

    def upload_file_to_blobstore_single(self, path):
        logger.info(f"In upload to blobstore, file: {path}")
        try:
            file = open(path, "rb")
        except Exception as e:
            logger.info("Could not find file: " + path)
            logger.info(e)
            exit(1)

        req = self.s.post(self.url + "/upload/single", files={"file": file}, headers={'Content-Type' : 'application/octet-stream'})

        logger.info(f"Got status: {req.status_code} for file {path}")
        logger.info(f"Got response:  {req.json()}")
        return req.json()["uploads"][0]


    def get_env_details(self, env_id, should_print=True):
        if should_print:
            logger.info("Getting env details...")

        details = self.s.get(self.url + f"/environment-repository/environments/{env_id}").json()

        if should_print:
            logger.info("Got details:")
            logger.info(json.dumps(details, indent=4))

        return details

    def delete_environment(self, env_id):
        logger.info(f"Deleting env with id {env_id}")
        delete_request = {"envId": env_id,
                          "deleteMetaData": True,
                          "deleteImage": True,
                          "force": False}
        response = self.s.post(self.url + "/EmilEnvironmentData/delete", json=delete_request)
        logger.info(f"Deletion status: {response.status_code}")

    def stop_component(self, comp_id):
        logger.info(f"Stopping component with id {comp_id}")
        response = self.s.get(self.url + f"/components/{comp_id}/stop", )
        logger.info(f"Stopping status: {response.status_code}")
        logger.info(f"Full response: {response.json()}")
        return response

    def start_component(self, request):
        response = self.s.post(self.url + f"/components/", json=request)
        return response

    def get_component_state(self, comp_id):
        logger.info(f"Getting component state for id {comp_id}")
        response = self.s.get(self.url + f"/components/{comp_id}/state", )
        logger.info(f"Response: {response}")
        return response

    # TODO force enable xpra for sikuli?
    def update_env(self, env, iso_id, archive="zero conf", should_print=True):
        logger.info("Preparing update request...")
        # FIXME will throw error when null/None, use gets everywhere
        update_json = {"envId": env["envId"],
                       "title": env["title"],
                       "description": env.get("description", ""),
                       "enablePrinting": env["enablePrinting"],
                       "enableRelativeMouse": env["enableRelativeMouse"],
                       "shutdownByOs": env["shutdownByOs"],
                       "os": env.get("os", ""),
                       "useXpra": env["useXpra"],
                       "useWebRTC": env["useWebRTC"],
                       "nativeConfig": env["nativeConfig"],
                       "containerEmulatorName": env["containerName"],
                       "containerEmulatorVersion": env.get("containerVersion", None),
                       "xpraEncoding": env["xpraEncoding"],
                       "linuxRuntime": env["linuxRuntime"],
                       "networking": env["networking"],
                       "hasOutput": env.get("hasOutput", "yes")
                       }

        drives = env["drives"]

        new_drives = []
        new_drive = {}
        cdrom_found = False
        drive_index = None
        for i, drive in enumerate(drives):
            if cdrom_found:
                new_drives.append(drive)
                continue
            if drive["type"] == "cdrom":
                cdrom_found = True
                new_drive = {
                    "iface": drive["iface"],
                    "bus": drive["bus"],
                    "unit": drive["unit"],
                    "type": "cdrom",
                    "filesystem": "ISO",
                    "boot": False,
                    "plugged": False,
                    "_id": iso_id
                }
                new_drives.append(new_drive)
                drive_index = i
            else:
                new_drives.append(drive)

        update_json["drives"] = new_drives

        drive_settings = {"objectId": iso_id, "objectArchive": archive, "driveIndex": drive_index,
                          "drive": new_drive}

        # if CONSTANTS.global_config.get("userId"):
        #     drive_settings["objectArchive"] = "user-" + CONSTANTS.global_config.get("userId")

        update_json["driveSettings"] = [drive_settings]

        if should_print:
            logger.info("Updating environment:")
            logger.info(json.dumps(update_json, indent=4))

        req = self.s.post(self.url + "/EmilEnvironmentData/updateDescription", json=update_json)
        logger.info(f"Updating description. Status: {req.status_code}, Response: {req.json()}")
        if req.json()["status"] != "0":
            logger.error("Failed to update env...")
            exit(1)

    def upload_iso(self, env_id, path_to_iso, label=None):
        logger.info("Uploading ISO...")
        iso_url = self.upload_file_to_blobstore(path_to_iso)

        if not label:
            label = f"Autogenerated ISO for Migration/Dump, env: {env_id}"

        import_request = {"label": label,
                          "files": [{
                              "deviceId": "Q495265",
                              "filename": path_to_iso,
                              "url": iso_url
                          }]}

        logger.info(f"Sending: {import_request}")
        req = self.s.post(self.url + "/objects/import", json=import_request)
        logger.info(f"Status code: {req.status_code}")
        logger.info(f"Got answer: {req.json()}")

        task_id = req.json()["taskId"]

        task_response = self.poll_until_done(task_id)

        return task_response["userData"]

    def delete_object(self, object_id):  # TODO zero conf / user ?!
        logger.info(f"Deleting Object with id {object_id}")
        url = self.url + "/objects/zero%20conf/" + object_id

        req = self.s.delete(url)

        if req.status_code == 200:
            logger.info("Successfully deleted object.")
        else:
            logger.info(f"Error deleting object with id {object_id}")

    def poll_until_done(self, task_id):
        while True:
            task_response = self.s.get(self.url + "/tasks/" + task_id)
            as_json = task_response.json()
            logger.info(as_json)
            task_id = as_json["taskId"]
            is_done = as_json["isDone"]

            if is_done:
                return as_json
            else:
                time.sleep(3)

    def upload_sikuli_script(self, component_id, sikuli_blobstore_url, library_entry_id=None):
        logger.info(f"Uploading Sikuli Script for component {component_id}")
        if not library_entry_id:
            body = {"componentId": component_id,
                    "blobStoreUrl": sikuli_blobstore_url}
        else:
            body = {"componentId": component_id,
                    "objectId": library_entry_id}

        return self.s.post(self.url + "/sikuli/api/v1/uploads",
                           json=body)

    def execute_sikuli_script(self, component_id, params=None, resolution=None):
        logger.info(f"Executing Sikuli Script for component {component_id}")
        url = self.url + "/sikuli/api/v1/execute"
        logger.info(f"Sending to: {url}")
        data = {"componentId": component_id,
                "headless": True}
        if params:
            data["parameters"] = params
        if resolution:
            data["resolution"] = resolution

        logger.info(f"Data: {data}")
        return self.s.post(url,
                           json=data)

    def get_compute_status(self, compute_id):
        return self.s.get(self.url + "/compute/" + compute_id)

    def start_compute(self, compute_request):
        return self.s.post(self.url + "/compute", json=compute_request)

    def get_network_environment(self, env_id):
        logger.info(f"Getting network environment for id {env_id}")
        url = self.url + f"/network-environments/{env_id}"
        return self.s.get(url).json()

    def check_sikuli_wait_queue(self, url):
        logger.info(f"Checking wait queue: {url}")
        return self.s.get(url).json()

    def stop_session(self, session_id):
        logger.info(f"Stopping (network) session {session_id}")
        return self.s.delete(self.url + f"/sessions/{session_id}")

    def stop_compute(self, session_id):
        logger.info(f"Stopping compute session {session_id}")
        resp = self.s.post(self.url + f"/compute/{session_id}/stop")
        logger.info(f"Got stop compute response: {resp.text}")
        return resp

    def get_all_envs(self, should_print=True):
        resp = self.s.get(self.url + "/environment-repository/environments")
        if should_print:
            logger.info("Got:", resp.json())
        return resp.json()


def start_client(url, token):
    global eaas_client
    logger.info("Starting Eaas Client")
    eaas_client = EaasClient(url, token)


def get_client() -> EaasClient:
    global eaas_client
    if not eaas_client:
        raise RuntimeError("Client was not set up!")
    return eaas_client


if __name__ == '__main__':
    logging.basicConfig(format="%(asctime)s%(msecs)03d %(message)s", datefmt="%d/%m/%Y %H:%M:%S,",
                        level=logging.DEBUG)
    # migration_example()

    a = EaasClient("https://au-demo.stabilize.app/emil",
                   "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJQSk9TcnllS3lrbEFxWUpWTVRwTENQYmU4b25OVUtUMk9DQ09nLWhuYXBNIn0.eyJleHAiOjE2NTQyMTUzMjksImlhdCI6MTY1NDE3OTMyOSwiYXV0aF90aW1lIjoxNjU0MTc5MzI5LCJqdGkiOiI1NTAxNTFhZi03MTdiLTRmOWMtODYxNi1jNDU2MjVlNDUzNmQiLCJpc3MiOiJodHRwczovL2F1LWRlbW8uc3RhYmlsaXplLmFwcC9hdXRoL3JlYWxtcy9tYXN0ZXIiLCJhdWQiOiJlYWFzIiwic3ViIjoiZjQ1MDAwMDEtZWFhNy00YTEwLTg3ZTctYmYyZThmZjE0MGU3IiwidHlwIjoiSUQiLCJhenAiOiJlYWFzIiwibm9uY2UiOiJpSGZpU1ExVXpxbVRfQW90b3NWYUF-clVubVJHQ1J1RCIsInNlc3Npb25fc3RhdGUiOiI5MWMyYjFlNC1iYzAxLTQzZjItOTkxNC1jZmM4MThkYzk3ZGQiLCJhdF9oYXNoIjoiYkQ4QlpFb2ZfVGdpdFcybHhlQnNldyIsImFjciI6IjEiLCJzX2hhc2giOiJVS3JHSmIwZk1JOUh4VGFxaEo4UVR3Iiwic2lkIjoiOTFjMmIxZTQtYmMwMS00M2YyLTk5MTQtY2ZjODE4ZGM5N2RkIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJhZG1pbiJ9.GXGms8hz4_utxTb5H3ohK_o8G7HQ7SPiWSSY-iqtmVPF7QeVwq0hL4ij1D7eNEbiP71zG8VPza4tLcKsJCpS7_KtsjwT-AnlFCNGnsv88vzA3QA6Yi_xq-mUZUO9J-MJZSi2TfXnOK_Ge97kK4kSUIFTS8uFxp_dSME8ci3kKlveC8VxbnQo97hazAnSATWBQRzacAuZOajC4rOZqjS-_oBwceBsNfMIsIr2QPN3j8N4wuRK13k2PWgXySyHB5G5gQn_PdVPhQYoCnyyl_C0xTe_ygGByIRh-nPD9oThgYg8gRZ90gBEiQKhwH8D5ttyw_WeNUrFBYJJiKfkVQgqTg")
    with open("envs.json") as envs:
        envs_dict = json.load(envs)

        for x in envs_dict:
            if x["title"] == "Test With Client":
                print("Found env, id", x["envId"])
                print(x["title"])
                a.delete_environment(x["envId"])
