import datetime
import json
import logging
import os.path
import pathlib
import tarfile
import threading
import time
from os.path import exists

import headless
from GLOBALS import EMIL_BASE_URL
from create_iso import add_dump_iso_to_env, add_autostart_win9x_iso_to_env, \
    add_autostart_winxp_iso_to_env, AutomationCategory, add_file_types_iso_to_env, \
    add_migration_iso_to_env
from git_repo import add_results_to_repo_and_push
from headless import start_component_with_compute

from eaas_client import get_client, start_client

# TODO api stuff to client
from outputExtraction import glob_extract
from tar_utils import tar_result

logger = logging.getLogger(__name__)


def run_environment_with_sikuli(env_id, sikuli_path, save_env=True, new_env_name=None, timeout=120,
                                sikuli_delay=35, script_already_uploaded=False, task_id="",
                                sikuli_only=False, custom_outputs=None, sikuli_params=None,
                                resolution=None):
    """
    Path is either local path to script or blobstore URL!!!
    :rtype: str
    """

    logger.info(f"Running environment {env_id} with sikuli script!")

    headless_thread = threading.Thread(target=_run_environment, daemon=True,
                                       args=(
                                           env_id, save_env, new_env_name, timeout, None, task_id))
    headless_thread.start()

    while headless.CURRENT_COMPONENT_ID is None:
        time.sleep(5)

    execute_sikuli_for_headless_component(sikuli_path, save_env, script_already_uploaded,
                                          sikuli_delay, sikuli_only, custom_outputs, task_id,
                                          sikuli_params, resolution, timeout=timeout)

    return headless.SIKULI_RESULT


# TODO refactor so that this and execute for headless can be reused here
def run_compute_for_component_with_sikuli(component_id, env_id, new_env_name, save_env, sikuli_path,
                                          script_already_uploaded,
                                          sikuli_delay, sikuli_only, custom_outputs, task_id,
                                          sikuli_params, resolution, timeout, session_id=None):
    compute_id = headless.start_compute_for_component(component_id, env_id, new_env_name, save_env,
                                                      timeout)

    headless_thread = threading.Thread(target=headless.wait_and_handle_compute_result, daemon=True,
                                       args=(
                                           component_id, compute_id, save_env, task_id, timeout))
    headless_thread.start()

    execute_sikuli_for_headless_component(sikuli_path, save_env, script_already_uploaded,
                                          sikuli_delay, sikuli_only, custom_outputs, task_id,
                                          sikuli_params, resolution, session_id=session_id,
                                          timeout=timeout)


def execute_sikuli_for_headless_component(sikuli_path, save_env, script_already_uploaded,
                                          sikuli_delay, sikuli_only, custom_outputs, task_id,
                                          sikuli_params=None, resolution=None,
                                          library_entry_id=None,
                                          session_id=None, timeout=None):
    def abort(error_msg):
        logger.info("--- Aborting ---")
        logger.info(error_msg)
        if not session_id:
            client.stop_component(headless.CURRENT_COMPONENT_ID)
        else:
            client.stop_session(session_id)
        exit(1)

    def check_timeout():
        now = datetime.datetime.now()
        diff = now - start_time
        logger.info(f"Diff: {diff.seconds // 60} minutes, {diff.seconds % 60} seconds. Provided timeout: {timeout}")
        return (diff.seconds / 60) > timeout

    client = get_client()

    write_to_status_file(task_id, f"Waiting {sikuli_delay}", save_env)

    logger.info(f"Got component ID for Sikuli Component: {headless.CURRENT_COMPONENT_ID}")
    logger.info(f"Task id: {task_id}, Custom Outputs: {custom_outputs}, params: {sikuli_params}")

    if not library_entry_id:
        if not script_already_uploaded:

            if not (sikuli_path.endswith(".tgz") or sikuli_path.endswith(".tar")):
                logger.info("Taring sikuli")
                tar_sikuli(sikuli_path)
                sikuli_path = "sikuli.tgz"
            logger.info("Uploading to blobstore")
            sikuli_blob = client.upload_file_to_blobstore(sikuli_path)
        else:
            logger.info("Path is already a blobstore URL...")
            sikuli_blob = sikuli_path
        logger.info("Uploading Sikuli Script to component container...")
        client.upload_sikuli_script(headless.CURRENT_COMPONENT_ID, sikuli_blob)
    else:
        client.upload_sikuli_script(headless.CURRENT_COMPONENT_ID, None, library_entry_id)

    logger.info(f"Sleeping for {sikuli_delay} seconds.")
    time.sleep(sikuli_delay)
    start_time = datetime.datetime.now()
    logger.info(f"Executing Sikuli Script, start: {start_time}.")
    execute_sikuli = client.execute_sikuli_script(headless.CURRENT_COMPONENT_ID,
                                                  sikuli_params, resolution)
    logger.info(f"Got Status Code for Execution: {execute_sikuli.status_code}")
    logger.info(f"Got Message: {execute_sikuli.json()}")

    sikuli_task_id = execute_sikuli.json()["taskId"]
    sikuli_wait_q_url = execute_sikuli.json()["waitQueueUrl"]
    write_to_status_file(task_id, "Executing Sikuli Script", save_env, sikuli_task_id)

    logger.info("Waiting for Sikuli to finish...")
    is_done_but_not_output_yet = False
    while headless.SIKULI_RESULT is None:
        time.sleep(10)
        logger.info("Checking sikuli status:")
        status = client.check_sikuli_wait_queue(sikuli_wait_q_url)
        logger.info(f"Got sikuli status: {status}")
        if status["hasError"]:
            logger.error("Sikuli script was not executed successfully!")
            write_to_status_file(task_id, "ERROR", save_env, sikuli_task_id, True)
            abort("ERROR, sikuli script was not executed successfully!")

        if is_done_but_not_output_yet:
            if check_timeout():
                abort("Timeout has been reached, while retrieving output.")

        if status["isDone"]:
            logger.info("Sikuli seems to be done, but result has not been retrieved yet.")
            is_done_but_not_output_yet = True

        if check_timeout():
            abort("Timeout has been reached before sikuli execution was done!")

    logger.info(f"(main) Got result from sikuli execution: {headless.SIKULI_RESULT}")
    write_to_status_file(task_id, "Handling result", save_env, sikuli_task_id)

    if sikuli_only and not save_env:
        logger.info("Sikuli only detected, result = files (not environment)")
        headless.SIKULI_RESULT = extract_sikuli_only(headless.SIKULI_RESULT, task_id,
                                                     custom_outputs)
    write_to_status_file(task_id, headless.SIKULI_RESULT, save_env, sikuli_task_id=sikuli_task_id)
    if session_id:
        client.stop_session(session_id)


def tar_sikuli(sikuli_path):
    logger.info(f"Checking type for: {sikuli_path}")
    ext = os.path.splitext(sikuli_path)[-1].lower()

    if ext.endswith(".py"):
        logger.info("Sikuli path leads to a sikuli python script.")
        # TODO check sikuli folder
        sikuli_path = os.path.dirname(sikuli_path)

    elif ext.endswith(".sikuli"):
        logger.info("Sikuli path leads to a .sikuli folder.")
        # TODO check py script in folder

    else:
        raise AttributeError(
            "Sikuli path is not properly formatted, either needs to be a .py script, or a .sikuli folder!")

    with tarfile.open("sikuli.tgz", "w:gz") as tar:
        tar.add(sikuli_path, arcname=os.path.normpath(sikuli_path).split(os.path.sep)[-1])
    logger.info("Done with taring!")


def run_environment_with_sikuli_autostart_and_iso(env_id, env_os, save_env, new_env_name,
                                                  iso_type: AutomationCategory, exec_location,
                                                  input_file=None, output_type=None,
                                                  input_folder="USERDATA", timeout=120,
                                                  sikuli_delay=60, task_id=None, delete=True,
                                                  parameter=""):
    logger.info("In run env with sikuli and iso...")
    sikuli_path = ""
    iso_id = ""

    # TODO tars here?
    if env_os == "os:windows:xp:32bit" or env_os == "os:windows:xp:64bit":
        sikuli_path = "sikuli-scripts/tgz_new/autostartInstaller_WinXP.tgz"
        iso_id = add_autostart_winxp_iso_to_env(env_id)

    elif env_os == "os:windows:95":
        logger.info("win 95 not implemented")
        sikuli_path = "sikuli-scripts/tgz_new/autostartInstaller_Win95.tgz"
        iso_id = add_autostart_win9x_iso_to_env(env_id)

    elif env_os == "os:windows:98":
        sikuli_path = "sikuli-scripts/tars/autostartInstaller_Win98.tar"
        iso_id = add_autostart_win9x_iso_to_env(env_id)

    if not sikuli_path:
        logger.info("Could not find sikuli path...")
        exit(1)

    # TODO resolution? Currently 1024 x 768 will be used as default
    logger.info(f"Sikuli delay: {sikuli_delay}")
    sikuli_env = run_environment_with_sikuli(env_id, sikuli_path, True,
                                             "TMP (Sikuli) from " + env_id,
                                             timeout=timeout,
                                             sikuli_delay=sikuli_delay)

    logger.info("Successfully installed Autoinstaller, will resume with ISO...")
    time.sleep(3)

    try:
        run_environment_with_iso(sikuli_env, save_env, new_env_name, iso_type, exec_location,
                                 input_file, output_type, input_folder, timeout, task_id,
                                 parameter=parameter)
    except Exception as e:
        logger.error("Error while executing run_environment_with_iso:")
        logger.exception(e)

    if delete:  # TODO make flag in config
        logger.info("Deleting ISO and temporary environment")
        get_client().delete_object(iso_id)
        get_client().delete_environment(sikuli_env)
    logger.info("Done with Autostart + ISO run!")


def run_environment_with_iso(env_id, save_env, new_env_name, iso_type, exec_location,
                             input_file=None,
                             output_type=None, input_folder="USERDATA", timeout=10,
                             task_id=None, parameter=""):
    if iso_type == AutomationCategory.DUMP:
        run_environment_with_dump(env_id, save_env, new_env_name, exec_location, input_file,
                                  timeout, task_id)
    elif iso_type == AutomationCategory.MIGRATION:
        run_environment_with_migration(env_id, save_env, new_env_name, exec_location, output_type,
                                       timeout, input_folder, parameter, task_id)
    elif iso_type == AutomationCategory.FILE_TYPES:
        run_environment_with_file_types(env_id, save_env, new_env_name, exec_location, input_file,
                                        timeout, task_id)
    else:
        raise AttributeError("Unknown ISO Type")


def run_environment_with_dump(env_id, save_env, new_env_name, exec_location, input_file,
                              timeout=10, task_id=""):
    obj_id = add_dump_iso_to_env(env_id, exec_location, input_file)
    result = _run_environment(env_id, save_env, new_env_name, timeout, obj_id, task_id)
    if not save_env:
        result = extract_dump_folder(result, task_id)
    write_to_status_file(task_id, result, save_env)


def run_environment_with_migration(env_id, save_env, new_env_name, exec_location, output_type,
                                   timeout=10, input_folder="USERDATA", parameter="", task_id=""):
    obj_id = add_migration_iso_to_env(env_id, exec_location, output_type, input_folder, parameter)
    result = _run_environment(env_id, save_env, new_env_name, timeout, obj_id, task_id)
    if not save_env:
        result = extract_migration_folder(result, task_id)
    write_to_status_file(task_id, result, save_env)


def run_environment_with_file_types(env_id, save_env, new_env_name, exec_location, input_file,
                                    timeout=10, task_id=""):
    obj_id = add_file_types_iso_to_env(env_id, exec_location, input_file)
    result = _run_environment(env_id, save_env, new_env_name, timeout, obj_id, task_id)
    if not save_env:
        result = extract_file_types(result, task_id)
    write_to_status_file(task_id, result, save_env)


def _run_environment(env_id, save_env, new_env_name, timeout, iso_id=None, task_id=None):
    time.sleep(3)
    headless_resp = start_component_with_compute(env_id, save_env, new_env_name, timeout,
                                                 task_id=task_id)
    time.sleep(3)

    if iso_id:
        get_client().delete_object(iso_id)
    if save_env:
        logger.info(f"DONE! New environment id: {headless_resp}")
    else:
        logger.info(f"DONE! Check output folder: {headless_resp}")

    return headless_resp


def write_to_status_file(task_id, result, save_env, sikuli_task_id=None, has_error=False):
    status_file = "/tmp-storage/automation/" + task_id + "/status.json"
    try:
        if not result:
            logger.warn("Result could not be retrieved properly!")
            result = "Could not retrieve result!"

        with open(status_file, "w") as result_file:
            if save_env:
                result_json = {"automationType": "environment", "result": result}
            else:
                result_json = {"automationType": "files", "result": result}
            if sikuli_task_id:
                result_json["sikuliTaskId"] = sikuli_task_id
            if has_error:
                result_json["hasError"] = True
            if headless.CURRENT_COMPONENT_ID:
                result_json["componentId"] = headless.CURRENT_COMPONENT_ID

            result_file.write(json.dumps(result_json, indent=4))
            logger.info(f"Wrote to: {status_file}")
    except Exception as e:
        logger.info(
            f"Could not write to {status_file}, (if you are running this in standalone mode, that is fine!)")


def extract_dump_folder(output_path, task_id):
    return extract(output_path, task_id, "Dump", "all_types_log.txt")


def extract_migration_folder(output_path, task_id):
    return extract(output_path, task_id, "Migration", "migration_log.txt")


def extract_file_types(output_path, task_id):
    return extract(output_path, task_id, "all_file_types.txt", "save_types_log.txt")


def extract_sikuli_only(output_path, task_id, outputs=None):
    if outputs:
        logger.info("Got custom outputs:" + str(outputs))
        glob_result = glob_extract(output_path, outputs)
        logger.info("Got glob result: " + str(glob_result))
        tar_file = tar_result(task_id, *glob_result)

    else:
        tar_file = tar_result(task_id, pathlib.Path(output_path))  # TODO partition?

    return get_client().upload_file_to_blobstore(tar_file)


def extract(output_path, task_id, result_path, log_path, autostart_log_path="autostart_log.txt"):
    try:
        result_path = pathlib.Path(output_path) / "partition-1" / result_path
        log_path = pathlib.Path(output_path) / "partition-1" / log_path
        autostart_log_path = pathlib.Path(output_path) / "partition-1" / autostart_log_path
        tar_file = tar_result(task_id, result_path, log_path, autostart_log_path)
        result = get_client().upload_file_to_blobstore(tar_file)
        add_results_to_repo_and_push(str(result_path))
        return result
    except Exception as e:
        logger.error("Error while extracting output:")
        logger.exception(e)


def example_1():
    env_id = "1ceafa90-912e-4153-9534-1961008359d9"  # Win XP + Sublime Installer
    save_env = True
    new_env_name = "Windows XP + Sublime Text 3 (Sikuli)"

    sikuli_path = "F:\OpenSLX\SikuliX\sublime.sikuli"

    run_environment_with_sikuli(env_id, sikuli_path, save_env, new_env_name, timeout=20)


def example_2():
    env_id = "cc1779b5-f9d6-4d08-8eca-81beab22d37f"
    exec_location = "C:\\Program Files\\CoffeeCup Software\\Coffee.exe"
    input_file = "examples\\example.html"

    run_environment_with_file_types(env_id, False, "whatever", exec_location, input_file)


def example_3():
    env_id = "cc1779b5-f9d6-4d08-8eca-81beab22d37f"
    exec_location = "C:\\Program Files\\CoffeeCup Software\\Coffee.exe"
    input_file = "examples\\example.html"
    new_env_name = "MR Testing: Coffee with File Dump"

    run_environment_with_dump(env_id, True, new_env_name, exec_location, input_file)


def example_4():
    env_id = "6884c039-8f26-4a2f-93af-556ed4d73f85"
    exec_location = "C:\\WINDOWS\\system32\\mspaint.exe"
    input_file = "examples\\example.png"

    run_environment_with_dump(env_id, False, "", exec_location, input_file, task_id="Example")


def example_4_1():
    env_id = "6884c039-8f26-4a2f-93af-556ed4d73f85"
    exec_location = "C:\\WINDOWS\\system32\\mspaint.exe"
    input_file = "examples\\example.png"

    run_environment_with_file_types(env_id, False, "", exec_location, input_file, timeout=7,
                                    task_id="Example")


def example_sublime_migration():
    env_id = "0dea922c-3e38-4a36-897b-0b8057df7f94"
    exec_location = "C:\\Program Files\\Sublime Text 3\\sublime_text.exe"

    run_environment_with_migration(env_id, False, "", exec_location, ".h", task_id="Example",
                                   parameter="Objective-C++", timeout=13)


def example_5():
    '''
    ENV: Windows XP + Sublime Text 3
    Install Autoinstaller via Sikuli,
    Start new Env with File_Types, get output
    '''

    run_environment_with_sikuli_autostart_and_iso("cfe5ccb4-abe5-4e72-b06a-6b325ea0f1ca",
                                                  "sikuli-scripts/autostartInstaller_WinXP.sikuli",
                                                  False,
                                                  "whatever", AutomationCategory.FILE_TYPES,
                                                  "C:\\WINDOWS\\system32\\mspaint.exe",
                                                  "examples\\example.png", timeout=10,
                                                  sikuli_delay=45)


if __name__ == '__main__':
    logging.basicConfig(format="%(asctime)s %(name)s - %(levelname)s: %(message)s",
                        datefmt="%d/%m/%Y %H:%M:%S,",
                        level=logging.INFO)
    start_client("https://b2f7ef23-db73-43a7-97fe-8531247b964a.test.emulation.cloud/emil",
                 "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJ5VVludDl3VHdKVGE2dS1TY1FZZXBlWFJtdG43c0dUY3ZlMDZYMzcyX0swIn0.eyJleHAiOjE2ODkxMTk0NzMsImlhdCI6MTY4OTA4MzQ3MywiYXV0aF90aW1lIjoxNjg5MDgzNDczLCJqdGkiOiIzOTZiZjUwMC02ZTBmLTQwOGQtOWVkOC1hN2RiYzU3YjAwNjYiLCJpc3MiOiJodHRwczovL2IyZjdlZjIzLWRiNzMtNDNhNy05N2ZlLTg1MzEyNDdiOTY0YS50ZXN0LmVtdWxhdGlvbi5jbG91ZC9hdXRoL3JlYWxtcy9tYXN0ZXIiLCJhdWQiOiJlYWFzIiwic3ViIjoiMTRlNjI5NDktMTY3MS00NTUyLTliMjMtZTU4NGRkMmI5MDhlIiwidHlwIjoiSUQiLCJhenAiOiJlYWFzIiwibm9uY2UiOiJEQzBCZWxMeFVsSzhpSEJ2RHowSTFza1ZfZWk2MDNEUiIsInNlc3Npb25fc3RhdGUiOiJhMDdjYmZiZi01ZTVhLTRjNmEtYmMyMS00MDlmMDM4NjUxZDAiLCJhdF9oYXNoIjoiNERjaElqYmIyeWYyVFFWaW12RUtrQSIsImFjciI6IjEiLCJzX2hhc2giOiJIQVNoWkE5RWtXZjdFRWw2X0ZKcUZBIiwic2lkIjoiYTA3Y2JmYmYtNWU1YS00YzZhLWJjMjEtNDA5ZjAzODY1MWQwIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJhZG1pbiJ9.YQhqe0MnnR0RWTMMqjwJ5_4TkdY6GUpU_C6MGaowFL014WS1wJSRU9mSYKhVs0ADzQc_OUdXvq8lQIn1BXROh32DHaBhBW7oUC0fPWvi5-3rklN-moRQg3w-MyYhB40T9SiXMJJIF-a450vW3azRd-wg7kt22UaxhqngDzc2MZ1CnGLXO4__DYF_ThtEKG6sGv7RzrtoYlGO6qR39BY82Iey3d0roY79cuy2ltjd3l9rc9ibYYuo-UQ9mT5EeU_Jo2SrDSPu6GLgQq8vq381kXOunhmgXuQKnzzG5vgsNSZuvzmJAja6qDEVVJxmuHUzRh6WrcdRqTDBrwfscJULiQ")
    env_id = "5290d4d8-3324-4a60-930e-b0ebd886d709"
    add_autostart_win9x_iso_to_env(env_id)