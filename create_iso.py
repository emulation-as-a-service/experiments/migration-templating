import logging
import os
import pycdlib

from enum import Enum

import GLOBALS
import eaas_client
from eaas_client import get_client
from environment_selection import EnvironmentSelector, SOFTWARE_NAME

logger = logging.getLogger(__name__)


class AutomationCategory(Enum):
    DUMP = 1
    MIGRATION = 2
    FILE_TYPES = 3


def build_eaas_autostart_file_migration(exec_location, output_type, output_folder="C:\\Migration",
                                        save_or_export="s", parameter=""):
    # FIXME if parameter is none, erro
    if not output_type.startswith("."):
        output_type = "." + output_type
    logger.info("Creating autostart file for migration...")
    sleep_timer = GLOBALS.task_config.get("sleepDuration", 1800)
    init_delay = GLOBALS.task_config.get("initialDelay", 40000)
    string_to_write = f'converterMultiFiles.exe "{exec_location}" {output_type} "{output_folder}" {save_or_export} {sleep_timer} {init_delay} {parameter}'
    logger.info("Creating autostart file with text:" + string_to_write)
    if parameter:
        string_to_write += f' "{parameter}"'

    with open("__eaas_autostart.txt", "w") as f:
        f.write(string_to_write)


def build_eaas_autostart_file_dump(exec_location, input_file, output_folder="C:\\Dump",
                                   save_or_export="s"):
    logger.info("Creating autostart file for dumping...")
    sleep_timer = GLOBALS.task_config.get("sleepDuration", 1800)
    init_delay = GLOBALS.task_config.get("initialDelay", 40000)
    string_to_write = f'createAllFiles.exe "{exec_location}" "{os.path.basename(input_file)}" "{output_folder}" {save_or_export} {sleep_timer} {init_delay}'
    logger.info("Creating autostart file with text:" + string_to_write)

    with open("__eaas_autostart.txt", "w") as f:
        f.write(string_to_write)


def build_eaas_autostart_file_file_types(exec_location, input_file=None, save_or_export="s"):
    logger.info("Creating autostart file for file type extraction...")
    sleep_timer = GLOBALS.task_config.get("sleepDuration", 1800)
    init_delay = GLOBALS.task_config.get("initialDelay", 40000)
    if not input_file:
        string_to_write = f'getAllSaveTypesNoOpen.exe "{exec_location}" {save_or_export}'
    else:
        string_to_write = f'getAllSaveTypes.exe "{exec_location}" "{os.path.basename(input_file)}" {save_or_export} {sleep_timer} {init_delay}'

    logger.info("Creating autostart file with text:" + string_to_write)

    with open("__eaas_autostart.txt", "w") as f:
        f.write(string_to_write)


def create_iso_migration(input_folder="USERDATA"):
    logger.info("Creating ISO...")
    iso_name = "migration.iso"
    iso = pycdlib.PyCdlib()

    iso.new(joliet=3)

    iso.add_file("__eaas_autostart.txt", joliet_path="/__eaas_autostart.txt")

    iso.add_file("autohotkey/ConverterV2/converterMultiFiles.exe",
                 joliet_path="/converterMultiFiles.exe")

    iso.add_directory(joliet_path="/USERDATA")

    for file in os.listdir(input_folder):
        iso.add_file(f"{input_folder}/{file}", joliet_path=f"/USERDATA/{file}")

    iso.write(iso_name)

    iso.close()
    return iso_name


def create_iso_dump(input_file):
    logger.info("Creating ISO...")
    iso_name = "dump.iso"
    iso = pycdlib.PyCdlib()

    iso.new(joliet=3)

    iso.add_file("__eaas_autostart.txt", joliet_path="/__eaas_autostart.txt")

    iso.add_file("autohotkey/CreateAllFileTypes/createAllFiles.exe",
                 joliet_path="/createAllFiles.exe")

    iso.add_file(f"{input_file}", joliet_path=f"/{os.path.basename(input_file)}")

    iso.write(iso_name)

    iso.close()
    return iso_name


def create_iso_file_types(input_file=None):
    iso = pycdlib.PyCdlib()
    iso_name = "filetypes.iso"
    iso.new(joliet=3)

    iso.add_file("__eaas_autostart.txt", joliet_path="/__eaas_autostart.txt")

    if input_file:
        iso.add_file(f"{input_file}", joliet_path=f"/{os.path.basename(input_file)}")
        iso.add_file("autohotkey/GetAllSaveTypes/getAllSaveTypes.exe",
                     joliet_path="/getAllSaveTypes.exe")

    else:
        iso.add_file("autohotkey/GetAllSaveTypes/getAllSaveTypesNoOpen.exe",
                     joliet_path="/getAllSaveTypesNoOpen.exe")

    iso.write(iso_name)

    iso.close()
    return iso_name


def create_iso_win9x_autostart(path=None):
    # TODO use path
    iso = pycdlib.PyCdlib()
    iso_name = "autostart.iso"
    iso.new(joliet=3)

    iso.add_file("autohotkey/AutoStartInstaller9x/starter.exe",
                 joliet_path="/starter.exe")
    iso.add_file("autohotkey/AutoStartInstaller9x/auto_setup_9x.bat",
                 joliet_path="/auto_setup_9x.bat")

    iso.write(iso_name)

    iso.close()
    return iso_name

#TODO these don't have to be created during runtime, isos can just be part of the repo?
def create_iso_winxp_autostart(iso_path="autostart.iso"):
    # TODO use path
    iso = pycdlib.PyCdlib()
    iso.new(joliet=3)

    iso.add_file("autohotkey/AutoStartInstallerXP32NoAutologin/starter.exe",
                 joliet_path="/starter.exe")
    iso.add_file("autohotkey/AutoStartInstallerXP32NoAutologin/auto_setup.bat",
                 joliet_path="/auto_setup.bat")
    iso.add_file(
        "autohotkey/AutoStartInstallerXP32NoAutologin/nssm.exe",
        joliet_path="/nssm.exe")

    iso.write(iso_path)

    iso.close()
    return iso_path


def add_dump_iso_to_env(env_id, exec_location, input_file):
    build_eaas_autostart_file_dump(exec_location, input_file)
    iso_path = create_iso_dump(input_file)
    return register_iso_with_env(env_id, iso_path)


def add_file_types_iso_to_env(env_id, exec_location, input_file):
    build_eaas_autostart_file_file_types(exec_location, input_file)
    iso_path = create_iso_file_types(input_file)
    return register_iso_with_env(env_id, iso_path)


def add_migration_iso_to_env(env_id, exec_location, output_type, input_folder="USERDATA",
                             parameter=""):
    build_eaas_autostart_file_migration(exec_location, output_type, parameter=parameter)
    iso_path = create_iso_migration(input_folder)
    return register_iso_with_env(env_id, iso_path)


def add_autostart_win9x_iso_to_env(env_id):
    iso_path = create_iso_win9x_autostart()
    return register_iso_with_env(env_id, iso_path, "win9xAutostart")


def add_autostart_winxp_iso_to_env(env_id):
    iso_path = create_iso_winxp_autostart()
    return register_iso_with_env(env_id, iso_path, "winXPAutostart")


def register_iso_with_env(env_id, iso_path, label=None):
    upload_data = get_client().upload_iso(env_id, iso_path, label)
    object_id = upload_data["objectId"]
    archive = upload_data.get("archive")
    if not archive:
        archive = "zero conf" # default to zero conf TODO?
    env_details = get_client().get_env_details(env_id)
    get_client().update_env(env_details, object_id, archive)
    return object_id


def file_types_example():
    # env_id = "ed776b5b-0a75-4185-ae2e-f9761e1760a3"
    # exec_location = "C:\\Program Files\\Sublime Text 3\\sublime_text.exe"
    # env_id = "fee4edee-1c7c-4b26-b8b3-8df611ca9ee1"
    # exec_location = "C:\\Program Files\\Ulead iPhoto Express\\Programs\\Ipe.exe"

    env_id = "b993f8d5-9e32-4093-8671-2cb24746d779"
    exec_location = "C:\\Program Files\\CoffeeCup Software\\Coffee.exe"

    build_eaas_autostart_file_file_types(exec_location, "examples\\example.html")
    iso_path = create_iso_file_types("examples\\example.html")
    object_id = get_client().upload_iso(env_id, iso_path)

    env_details = get_client().get_env_details(env_id)

    get_client().update_env(env_details, object_id)


def migration_example():
    env_selector = EnvironmentSelector()

    input_types = env_selector.get_input_types_for_userdata()
    output_type = ".json"

    env = env_selector.select_environment(input_types, output_type)

    if not env:
        logger.info("No suitable environment was found!")
        exit(1)

    build_eaas_autostart_file_migration(env.exec_location, output_type)
    create_iso_migration()

    object_id = get_client().upload_iso(env.id, "migration.iso")

    env_details = get_client().get_env_details(env.id)

    get_client().update_env(env_details, object_id)


def dump_examples():
    examples = {1: ("iPhoto Express", "examples\\example.png"),
                2: ("AutoCAD 14", "examples\\example.dwg"),
                3: ("CoffeeCup Software", "examples\\example.html"),
                4: ("Sublime Text", "examples\\example.txt"),
                5: ("Microsoft Paint", "examples\\example.png"), }

    name, input_file = examples[4]

    env_selector = EnvironmentSelector()
    env = env_selector.get_env_by_key(SOFTWARE_NAME, name)

    add_dump_iso_to_env(env.id, env.exec_location, input_file)


if __name__ == '__main__':
    logging.basicConfig(format="%(asctime)s%(msecs)03d %(message)s", datefmt="%d/%m/%Y %H:%M:%S,",
                        level=logging.DEBUG)

    # eaas_client.start_client("https://au-demo.stabilize.app",
    #                         "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJQSk9TcnllS3lrbEFxWUpWTVRwTENQYmU4b25OVUtUMk9DQ09nLWhuYXBNIn0.eyJleHAiOjE2NTMwNjg5OTgsImlhdCI6MTY1MzAzMjk5OCwiYXV0aF90aW1lIjoxNjUzMDMyOTk4LCJqdGkiOiI2MDc0MzVlNS1mYjY1LTRhMTUtYTY5MC01MTM3YzEyNGMzZTMiLCJpc3MiOiJodHRwczovL2F1LWRlbW8uc3RhYmlsaXplLmFwcC9hdXRoL3JlYWxtcy9tYXN0ZXIiLCJhdWQiOiJlYWFzIiwic3ViIjoiZjQ1MDAwMDEtZWFhNy00YTEwLTg3ZTctYmYyZThmZjE0MGU3IiwidHlwIjoiSUQiLCJhenAiOiJlYWFzIiwibm9uY2UiOiJwdWlzOURrNFA1VGtLdWs5YUZtRzlpVlZJdzIwNEJkLiIsInNlc3Npb25fc3RhdGUiOiI3YzFkZjlmNC1hZjJhLTQ1NmItYTdmZS0wYjk2NmI2YmNlY2QiLCJhdF9oYXNoIjoiZTQyN1IxV2FrbEZEWlptWDdKUmRTZyIsImFjciI6IjEiLCJzX2hhc2giOiJjY0YzZHpxN1pYQU91OGJiNi0wS05BIiwic2lkIjoiN2MxZGY5ZjQtYWYyYS00NTZiLWE3ZmUtMGI5NjZiNmJjZWNkIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJhZG1pbiJ9.IZCJsZd-iYMEqCdfFvT8GT4X4KOBQxSj45qOkv0MDeBryJ85TXXywtxcm1B9XfVHvdPoN79TSAsJ-00vWXlW6en6ruFX0hUCcnJmTB3HeMJeUhr85e60d0TlCUCf_9cQI7tKQYawLZ7QQ5uq4Ox2pBKgmCDBSk1FH_RKJGYBgqmJ-ctpBSC4V5Gt5JiXRwulsdpdluhJVfUqZU_05HK5Y1DsTzwpB2f3NLWCwpoNYSx8xGBlGlhwrhC5LcwaxJOqkOIpL2dviimmbuS1ZmFekoq3MUS3OODUv8IwSVxQTHY1yCG5LBRA79tID5oWZH2uAHG0d-eCnmWNTMyqtIIHRw")

    # eaas_client.start_client(
        # "https://70c9826a-71a6-46d2-a2cc-7bd7c9c0b8b2.test.emulation.cloud/emil", None)


    eaas_client. start_client("https://b2f7ef23-db73-43a7-97fe-8531247b964a.test.emulation.cloud/emil",
                 "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJ5VVludDl3VHdKVGE2dS1TY1FZZXBlWFJtdG43c0dUY3ZlMDZYMzcyX0swIn0.eyJleHAiOjE2ODkxOTA4ODIsImlhdCI6MTY4OTE1NDg4MiwiYXV0aF90aW1lIjoxNjg5MTU0ODgyLCJqdGkiOiJiZDhkY2E3Ny1kNzY5LTQ3MDItODQyZS04NGUwMTdjOTU0ZGYiLCJpc3MiOiJodHRwczovL2IyZjdlZjIzLWRiNzMtNDNhNy05N2ZlLTg1MzEyNDdiOTY0YS50ZXN0LmVtdWxhdGlvbi5jbG91ZC9hdXRoL3JlYWxtcy9tYXN0ZXIiLCJhdWQiOiJlYWFzIiwic3ViIjoiMTRlNjI5NDktMTY3MS00NTUyLTliMjMtZTU4NGRkMmI5MDhlIiwidHlwIjoiSUQiLCJhenAiOiJlYWFzIiwibm9uY2UiOiJfQTIzVnRfNlF4dTZnYmN2cGRvTkdfS2F6fnN1c0ZSLiIsInNlc3Npb25fc3RhdGUiOiI3YjRhYzM1MS04YWJmLTQyYTgtYjcyYi0xYjJmMjI5NzdmNWEiLCJhdF9oYXNoIjoiZGNvaFZnbmVBeG1RaDI1WFB1YWRMUSIsImFjciI6IjEiLCJzX2hhc2giOiJmYmRIWGc1QjdaNmhLSWxUWmNJOHV3Iiwic2lkIjoiN2I0YWMzNTEtOGFiZi00MmE4LWI3MmItMWIyZjIyOTc3ZjVhIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJhZG1pbiJ9.QeVK6Cu7-MP5qR4dWCECl6xGvY_fdfzBeM2muza30KYoj_xf199loGV1iKUzyBgNsXM5XzTsJQr41xyQcJ51QVQ9BML4Reu52C2mqEeRxQc7TwShk7VxbBUkJTpsuq8U63lYGj4qt5YTKelSIMZrdPF0oLgYxRkDQnSj369ObfcFgkL63WIy5tmNKF44wucAxvyG-w3J05OC8uHmQUBG6hGpF8of3iwerou0HKJBXYnX_-37f5Z36o_QEjozwWdawSyspkV4AeWSw1G-9trHM6K3M2gXp8yk25bltQvCgWPIvwPwqkfSX-aRL6qE5MYvlglWFtlz1IvhBJAYTeNZTQ")
    GLOBALS.task_config["sleepDuration"] = 3000
    GLOBALS.task_config["initialDelay"] = 35000

    # add_dump_iso_to_env("7ddc3032-68e5-4bfa-8096-a3ae06cb0010", "C:\Program Files\Sublime Text 3\sublime_text.exe", "examples/example.txt")
    # add_file_types_iso_to_env("7ddc3032-68e5-4bfa-8096-a3ae06cb0010", "C:\Program Files\Sublime Text 3\sublime_text.exe", "examples/example.txt")
    # add_file_types_iso_to_env("7ddc3032-68e5-4bfa-8096-a3ae06cb0010", "C:\WINDOWS\system32\mspaint.exe", "examples/example.png")

    add_file_types_iso_to_env("03a4a2ca-27d0-4a73-8658-404e7f6cbd94", "C:\Program Files\CoffeeCup Software\Coffee.exe", "examples/example.html")


    # add_migration_iso_to_env("2f87ad99-f993-4156-aa90-2eab8691e836",  "C:\Program Files\Sublime Text 3\sublime_text.exe", ".html", input_folder="USERDATA", parameter="HTML")

    # add_file_types_iso_to_env("e63a66e2-424e-41e1-ac6b-27a2e94f0717", "C:\Program Files\CoffeeCup Software\Coffee.exe", "examples/example.html")

    # migration_example()
    # file_types_example()
    # add_migration_iso_to_env("fee4edee-1c7c-4b26-b8b3-8df611ca9ee1", "C:\\Program Files\\Ulead iPhoto Express\\Programs\\Ipe.exe", ".bmp")
    # add_migration_iso_to_env("ed776b5b-0a75-4185-ae2e-f9761e1760a3", "C:\\Program Files\\Sublime Text 3\\sublime_text.exe", ".c")
    # add_migration_iso_to_env("c48eac77-a3ea-4f49-99dc-9cbbcf5746cf", "C:\\Program Files\\Ulead iPhoto Express\\Programs\\Ipe.exe", ".jpg", input_folder="USERDATA2")
    # add_autostart_win9x_iso_to_env("402312b0-9f67-47b4-b7b6-03ae8d791559")
    # add_dump_iso_to_env("4e26aeaa-fd9a-453b-84e2-7e9e4df44771", "C:\\WINDOWS\\system32\\mspaint.exe", "examples/example.png")
    # add_dump_iso_to_env("c48eac77-a3ea-4f49-99dc-9cbbcf5746cf", "C:\\Program Files\\Ulead iPhoto Express\\Programs\\Ipe.exe", "examples/example.png")
    # add_dump_iso_to_env("4c19b9da-2665-43f5-83cd-4c72fb8b0f8e", "C:\\WINDOWS\\system32\\mspaint.exe", "examples/example.png")
    # create_iso_winxp_autostart("win_xp_autostart.iso")
