import json
import logging
import os
import re

OPEN_FORMATS = "Open File Format(s)"
DEFAULT_SAVE_FORMAT = "Default Save File Format"
ENVIRONMENT_ID = "EaaSI Environment UUID"
IMPORT_FILE_FORMAT = "Import File Formats"
OTHER_SAVE_FORMATS = "Other Save File Format(s)"
EXECUTABLE_PATH = "Executable Location"
SOFTWARE_NAME = "Software Product Name"

logger = logging.getLogger(__name__)


class Environment:

    def __init__(self):
        self.id = ""
        self.name = ""
        self.input_formats = []
        self.output_formats = []
        self.exec_location = ""
        self.as_dict = {}


class EnvironmentSelector:

    def __init__(self):
        self.envs = []

        self.initialize_environments()

    def get_types_regex(self, supported_types_string):

        # logger.info("\tChecking regex")
        matches = re.findall('\.[\w\-\.]+', supported_types_string)
        # logger.info("\t Found:", matches)
        return matches

    def initialize_environments(self):
        with open("environments.json") as json_file:
            envs_as_json = json.load(json_file)

            if "environments" not in envs_as_json.keys():
                raise ValueError("JSON did not contain field 'environments'.")
            potential_envs = envs_as_json["environments"]

            for potential_env in potential_envs:
                # logger.info("Checking", potential_env["NSRL Name"])

                if not {OPEN_FORMATS, ENVIRONMENT_ID,
                        DEFAULT_SAVE_FORMAT} <= potential_env.keys():
                    continue

                e = Environment()

                e.input_formats = self.get_types_regex(potential_env[OPEN_FORMATS])

                if IMPORT_FILE_FORMAT in potential_env.keys():
                    e.input_formats.extend(
                        self.get_types_regex(potential_env[IMPORT_FILE_FORMAT]))

                e.output_formats = self.get_types_regex(potential_env[DEFAULT_SAVE_FORMAT])

                if OTHER_SAVE_FORMATS in potential_env.keys():
                    e.output_formats.extend(
                        self.get_types_regex(potential_env[OTHER_SAVE_FORMATS]))

                # TODO import types, default + other save file format

                e.exec_location = potential_env[EXECUTABLE_PATH]

                e.name = potential_env[SOFTWARE_NAME]
                e.id = potential_env[ENVIRONMENT_ID]
                e.as_dict = potential_env
                self.envs.append(e)

    def select_environment(self, input_type, output_type):
        for env in self.envs:
            if all(x in env.input_formats for x in input_type) \
                    and output_type in env.output_formats:
                return env

        return None

    def get_env_by_key(self, what_key, key) -> Environment:
        for env in self.envs:
            if env.as_dict[what_key] == key:
                logger.info("Returning env with id:", env.id)
                return env

        raise ValueError("No matching environment found!")

    def get_input_types_for_userdata(self):
        types = set()
        for file in os.listdir("test_data/USERDATA"):
            input_type = os.path.splitext(file)[1]
            types.add(input_type)

        return list(types)
