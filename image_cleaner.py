import os

import eaas_client

from eaas_client import get_client


def get_url(image_id):
    return f"http://localhost:8080/emil/environment-repository/images/{image_id}/url"


def get_all_backing_files(img_id):
    ids_not_to_delete.add(img_id)
    stream = os.popen(f'qemu-img info {get_url(img_id)}')
    output = stream.readlines()

    for line in output:
        if line.startswith("backing file: "):
            new_id = line[14:].strip()
            new_id = new_id[:36]
            print("Backing file id:", new_id)
            get_all_backing_files(new_id)


if __name__ == '__main__':

    # logging.basicConfig(format="%(asctime)s%(msecs)03d %(message)s", datefmt="%d/%m/%Y %H:%M:%S,",
    #                     level=logging.DEBUG)

    eaas_client.start_client(
        "https://b2f7ef23-db73-43a7-97fe-8531247b964a.test.emulation.cloud/emil",
        "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJ5VVludDl3VHdKVGE2dS1TY1FZZXBlWFJtdG43c0dUY3ZlMDZYMzcyX0swIn0.eyJleHAiOjE2ODg0MTEzODgsImlhdCI6MTY4ODM3NTM4OCwiYXV0aF90aW1lIjoxNjg4Mzc1Mzg4LCJqdGkiOiJhZjI3NzU4YS02MGMxLTRlNTgtYTQ4ZS01YzQwODQyODg3NzciLCJpc3MiOiJodHRwczovL2IyZjdlZjIzLWRiNzMtNDNhNy05N2ZlLTg1MzEyNDdiOTY0YS50ZXN0LmVtdWxhdGlvbi5jbG91ZC9hdXRoL3JlYWxtcy9tYXN0ZXIiLCJhdWQiOiJlYWFzIiwic3ViIjoiMTRlNjI5NDktMTY3MS00NTUyLTliMjMtZTU4NGRkMmI5MDhlIiwidHlwIjoiSUQiLCJhenAiOiJlYWFzIiwibm9uY2UiOiJ3cFVYTkhDTHkzbjdLeFNFRFpjaWVLa2hGTVUzLjhtNCIsInNlc3Npb25fc3RhdGUiOiIyZmJiZmFmMy05MDVhLTQzNzMtYmUzYy1iOTY0NjViYzFjMjEiLCJhdF9oYXNoIjoieUQzRy1pLVIzQkdvUlZVT19QS2l5USIsImFjciI6IjEiLCJzX2hhc2giOiJKQnBzTzlVNlJWalNnZEk3RjlodUl3Iiwic2lkIjoiMmZiYmZhZjMtOTA1YS00MzczLWJlM2MtYjk2NDY1YmMxYzIxIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJhZG1pbiJ9.JTteJctjwle9mR_7e0CIkeGZTWPgIv7ZF_IFHalW0T7uEayrK9RWGz_ogHcuPYhtlAXGFRQI2_pc4DaMEn48tH_ebX9igAKcAfvGtPcHGKlv_gxU8uvntD3LvEgPkrICJSdZ6kp7Jfd97bB9-tLNfdHaatpL1ZrNFN4YquBkJKxqPWs36WwbmYlunjJ2oEcrY4EfrX4TmR5renfU-BQMY8J3z2flU27JeH60bi7LlGrKbVEipVrgop_8danzMrA1U856lWk93W7DYKK3nqtRHPMi5_LkjkMqIQNBfAusOSa4jIg4HUIOy-EWjCmgYgfE2UwtHET1pDTpUj8y1-uKHA")

    image_path = "/eaas/minio/data/image-archive/images"

    client = get_client()

    envs = client.get_all_envs(False)

    ids_not_to_delete = set()

    for env in envs:
        print("-----------------")
        print("Env:", env["envId"])
        try:
            details = client.get_env_details(env["envId"], False)

        except Exception as e:
            print(f"Could not get details for env: {env}, skipping ...")
            continue

        drives = details["drives"]

        for drive in drives:
            if drive["type"] == "disk":
                data = drive.get("data", None)
                if data:
                    img_id = data[10:]
                    print("Found hard drive disk, Image to check:", img_id)
                    get_all_backing_files(img_id)

    print("-------------------------------")
    print("DONE!")
    print("-------------------------------")
    print("Used images:")
    print(ids_not_to_delete)
