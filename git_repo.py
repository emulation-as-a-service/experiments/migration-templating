import datetime
import json
import logging
import os
import pathlib
from shutil import copy2

from git import Repo

# TODO config from main?
import GLOBALS

git_url = ""
file_repo = ""
logger = logging.getLogger(__name__)


def init_repo():
    global git_url
    global file_repo
    config = GLOBALS.global_config
    git_url = config["repoUrl"]
    file_repo = config["repoDirectory"]
    logger.info(f"Initializing git repo {git_url} at {file_repo}")

    repo = None
    if not os.path.exists(file_repo):
        logger.info(f"Repository does not exist, cloning into {file_repo}")
        repo = Repo.clone_from(git_url, file_repo)

    else:
        logger.info("Repository exists, pulling...")
        repo = Repo(file_repo)

        repo.remotes.origin.pull()


def git_push(output_path):
    try:
        logger.info("Pushing to repo")
        repo = Repo(file_repo)
        repo.remotes.origin.pull()
        repo.git.add(all=True)
        repo.index.commit(f"Adding files at {output_path}.")
        repo.remotes.origin.push()
        # x = origin.push(refspec="main:main")
    except Exception as e:
        logger.error(f'Some error occurred while pushing the code: {e}')


def add_results_to_repo_and_push(result_path):
    if not GLOBALS.task_config:
        with open("config_1.json") as json_file:
            GLOBALS.task_config = json.load(json_file)

    exec_path = GLOBALS.task_config.get("executableLocation", None)
    env_os = GLOBALS.task_config.get("os", None)
    category = GLOBALS.task_config.get("automationType")

    os_dir = env_os.replace(":", "_").replace(" ", "_")
    exec_dir = exec_path.replace(":", "").replace("\\", "_").replace(" ", "_")

    repo_exec_path = file_repo + "/" + exec_dir
    repo_os_path = repo_exec_path + "/" + os_dir

    output_path = ""

    if category == "dump":
        output_path = repo_os_path + "/" + "Dump"
    elif category == "migration":
        output_path = repo_os_path + "/" + "Migration"
    elif category == "fileTypes":
        output_path = repo_os_path + "/" + "FileFormats"

    if not os.path.exists(output_path):
        os.makedirs(output_path)
    elif category != "migration":
        logger.info(f"Directory for {output_path} already exists!")
        if os.listdir(output_path):
            logger.info("Data already found!")
            return  # TODO how to handle? overwrite, add?
        else:
            logger.info(
                "It is empty however - this might be due to a failed previous automation task, resuming...")

    if category == "migration":
        output_format = GLOBALS.task_config["outputFileFormat"]

        parameter = GLOBALS.task_config.get("outputParameter", None)

        if parameter:
            output_format = parameter.replace(" ", "_") + "_" + output_format

        output_path = output_path + "/" + datetime.datetime.now().strftime(
            "%Y_%m_%d_%H_%M_") + output_format.replace(".", "")
        os.makedirs(output_path)

    if os.path.isdir(result_path):
        for file_name in os.listdir(result_path):
            copy2(result_path + "/" + file_name, output_path)
    else:
        copy2(result_path, output_path)

    git_push(output_path)


if __name__ == '__main__':
    init_repo()

    add_results_to_repo_and_push(
        "output_2022_05_06_17_06_32_6884c039-8f26-4a2f-93af-556ed4d73f85/partition-1/all_file_types.txt")
