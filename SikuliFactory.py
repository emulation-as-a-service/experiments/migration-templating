import logging
import time

from execute import run_environment_with_sikuli

logger = logging.getLogger(__name__)


class SikuliFactory:

    def __init__(self, env_id, save_env, new_env_name, timeout=30, sikuli_delay=30,
                 delete_tmp_envs=True):
        self.env_id = env_id
        self.save_env = save_env
        self.new_env_name = new_env_name
        self.timeout = timeout
        self.sikuli_delay = sikuli_delay
        self.delete_tmp_envs = delete_tmp_envs

        self.sikuli_paths = []

    def add_sikuli_script(self, path):
        self.sikuli_paths.append(path)

    def add_sikuli_scripts(self, paths):
        self.sikuli_paths.extend(paths)

    def execute(self):
        logger.info("Executing sikuli scripts...")

        env_to_execute = self.env_id

        for sik in self.sikuli_paths:
            logger.info(f"[Sikuli Factory] Executing {sik} in environment {env_to_execute}.")
            env_to_execute = run_environment_with_sikuli(env_to_execute, sik, self.save_env,
                                                         self.new_env_name, self.timeout,
                                                         self.sikuli_delay)
            time.sleep(5)

            #  TODO delete envs here? but does this destroy the resulting env?
