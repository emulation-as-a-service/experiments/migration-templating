import logging
import os
import pathlib

logger = logging.getLogger(__name__)


def glob_extract(output_path, outputs):
    to_extract = []

    for partition in [f.path for f in os.scandir(output_path) if f.is_dir()]:

        logging.info(f"Globbing in partition: {partition}")
        for o in outputs:
            for p in pathlib.Path(partition).rglob(o):
                logger.info(f"Got file: {p.name}")
                to_extract.append(p)

    return list(set(to_extract))


if __name__ == '__main__':
    logging.basicConfig(format="%(asctime)s%(msecs)03d %(message)s", datefmt="%d/%m/%Y %H:%M:%S,",
                        level=logging.DEBUG)
    glob_extract("outputTest",
                 ["*.txt"])
