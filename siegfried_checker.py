import json
import logging

logger = logging.getLogger(__name__)

if __name__ == '__main__':

    unknown_files = []
    known_files = []
    multiple_type_files = []

    with open("test_data/sf2.json", encoding="UTF-8") as json_file:
        sf_json = json.load(json_file)
        print(sf_json)

    for file in sf_json["files"]:
        matches = file["matches"]
        if len(matches) == 1:

            if matches[0]["id"] == "UNKNOWN":
                unknown_files.append(file)
            else:
                known_files.append(file)

        else:
            multiple_type_files.append(file)

    with open("test_data/sf_sorted.json", "w+") as new_json:

        sf_json.pop("files")
        sf_json["unknown_files"] = unknown_files
        sf_json["known_files"] = known_files
        sf_json["multiple_type_files"] = multiple_type_files

        json.dump(sf_json, new_json, indent=4)
