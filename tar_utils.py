import logging
import pathlib
import tarfile

logger = logging.getLogger(__name__)


def tar_result(task_id, *directory_paths):
    # TODO make usable when not running on server as well
    tar_path = "/tmp-storage/automation/" + task_id + "/result.tgz"

    with tarfile.open(tar_path, "w:gz") as tar:
        for p in directory_paths:
            try:
                tar.add(p, arcname=pathlib.PurePath(p).name)
            except Exception as e:
                logger.error(f"Could not add {p} to tar:")
                logger.exception(e)

    return tar_path


if __name__ == '__main__':
    # tar_result("output_2022_03_30_11_41_29_cc1779b5-f9d6-4d08-8eca-81beab22d37f_", "ABCDEF")
    print("test")
