import os
import subprocess

imgs_not_to_delete = {
    "ac7519d4-b284-4f6e-8496-01fc42b84e60",
    "80f67ad3-2a25-4f67-b32b-6a6003675fc3",
    "e7cb5739-571e-4d8e-93f8-00018fe2fb08",
    "f464fa8a-e8a4-4be9-a799-8dd1e42e8831",
    "3f7e2482-9704-4dfa-a88f-65c6952c44b7",
    "9d4eff67-733c-4745-9883-447476b3f9ca",
    "43a5058b-9c83-4abb-b514-c40cc743bf37",
    "13df9cc5-2f0c-4ecd-b728-d04577045bbd",
    "26966d54-44c2-41ae-8783-4dd1507cc714",
    "bafeffae-fdc0-4b5c-8910-5c822a690c52",
    "36960bae-3465-4e64-bd8a-fdb79c2f390e",
    "48bb5a32-4ea8-419a-b8b1-b576a18c9347",
    "0d360ade-49a2-4ec3-b4ab-633f1d078a90",
    "795360da-9ac1-4317-9aa7-3641255859dd",
    "769ef728-31cc-47d6-9dc5-5f1c9efd8573",
    "624a939b-a339-47f8-9fd5-f2688b66ea0a",
    "973271bb-0f40-4670-9726-3dfe61687c47",
    "053eba91-fd4c-44e1-a483-04a413b2605f",
    "52a52233-d71d-45b8-8b85-06e036d34553",
    "0f8d9253-89d5-4815-9a03-3b9c1c8137b7",
    "b9b751fe-a542-4b09-b5b9-049bf1189239",
    "094ed927-12fb-4969-8cd5-f1c21fe1537e",
    "0e7adee0-a506-4598-a4fb-8a2344ec7cf0",
    "9b94b346-7d21-4a43-bfb0-3a1c4018d149",
    "b41e27a9-6a1c-44df-9c4c-090f5b3de1ca",
    "4c082050-e42a-4e77-a6a7-f04368b44a65",
    "708aecd4-99ea-4a0e-ad57-eaccca305a35"}

image_path = "/mnt/minio/data/image-archive/images"

md_path = "/mnt/minio/data/image-archive/metadata/images"

for file in os.listdir(image_path):
    print(file)

    if file not in imgs_not_to_delete:

        full_path = os.path.join(image_path, file)
        print("Deleting:", full_path)
        subprocess.run(["sudo", "rm", "-rf", full_path], check=True)

        md_full = os.path.join(md_path, file)

        if os.path.exists(md_full):
            print("Deleting Metadata:", md_full)

            subprocess.run(["sudo", "rm", "-rf", md_full], check=True)